const express = require('express');
const message = require('./app/message');
const cors = require('cors');
const fileDB = require('./fileDb');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;
app.use('/thread', message);

fileDB.init();
app.listen(port, () => {
	console.log(`Server started on ${port} port!`);
});