const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const fileDB = require('../fileDb');
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},

	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
	const products = fileDB.getItems();
	res.send(products);
});

router.post('/', upload.single('image'), (req, res) => {
	if (!req.body.message) {
		return res.status(404).send({error: "Data not valid"});
	}

	const message = {
		author: req.body.author || "Anonymous",
		message: req.body.message,
		image: req.body.image,
	};

	if (req.file) {
		message.image = req.file.filename;
	}

	const newProduct = fileDB.addItem(message);

	res.send(newProduct);
});

module.exports = router;