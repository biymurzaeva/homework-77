import {Route, Switch} from "react-router-dom";
import Thread from "./containers/Thread/Thread";

const App = () => (
  <Switch>
    <Route path="/" exact component={Thread}/>
    <Route path="/thread" component={Thread}/>
  </Switch>
);

export default App;
