import axios from "axios";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const ADD_MESSAGE_REQUEST = 'ADD_MESSAGE_REQUEST';
export const ADD_MESSAGE_SUCCESS = 'ADD_MESSAGE_SUCCESS';
export const ADD_MESSAGE_FAILURE = 'ADD_MESSAGE_FAILURE';

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, payload: messages});
export const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const addMessageRequest = () => ({type: ADD_MESSAGE_REQUEST});
export const addMessageSuccess = () => ({type: ADD_MESSAGE_SUCCESS});
export const addMessageFailure = () => ({type: ADD_MESSAGE_FAILURE});

export const fetchMessages = () => {
	return async dispatch => {
		try {
			dispatch(fetchMessagesRequest());
			const response = await axios.get('http://127.0.0.1:8000/thread');
			dispatch(fetchMessagesSuccess(response.data));
		} catch (e) {
			dispatch(fetchMessagesFailure());
		}
	};
};

export const addMessage= threadData => {
	return async dispatch => {
		try {
			dispatch(addMessageRequest());
			await axios.post(`http://127.0.0.1:8000/thread`, threadData);
			dispatch(addMessageSuccess());
		} catch (e) {
			dispatch(addMessageFailure());
			throw e;
		}
	};
};