import React from 'react';

import {apiURL} from "../../config";
import {Grid, makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
	img: {
		maxWidth: theme.spacing(50)
	},
}));

const Message = ({author, message, image}) => {
	const classes = useStyles();

	let avatarImage = null;

	if (image) {
		avatarImage = apiURL + '/uploads/' + image;
	}

	if (image === "null") {
		return (
			<Grid item xs>
				<h4>{author}</h4>
				<p>{message}</p>
			</Grid>
		);
	}

	return (
		<Grid item xs>
			<h4>{author}</h4>
			<p>{message}</p>
			<img
				className={classes.img}
				src={avatarImage}
				alt="avatar"
			/>
		</Grid>
	);

};

export default Message;
