import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addMessage, fetchMessages} from "../../store/actions/threadAction";

import MessageForm from "../MessageForm/MessageForm";
import Message from "../../components/Message/Message";
import {Container, Grid} from "@material-ui/core";

const Thread = () => {
	const dispatch = useDispatch();
	const messages = useSelector(state => state.messages.messages);


	useEffect( () => {
		dispatch(fetchMessages());
	}, [dispatch]);

	const onSubmit = async messageData => {
		await dispatch(addMessage(messageData));
		dispatch(fetchMessages());
	};

	return (
		<Container>
			<Grid container direction="column" spacing={2}>
				{messages.map(message => (
					<Message
						key={message.id}
						id={message.id}
						author={message.author}
						message={message.message}
						image={message.image}
					/>
				))}
				<MessageForm
					onSubmit={onSubmit}
				/>
			</Grid>
		</Container>
	);
};

export default Thread;