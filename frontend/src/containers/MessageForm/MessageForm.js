import {useState} from "react";
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";
import FileInput from "../../UI/FileInput/FileInput";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
}));

const MessageForm = ({onSubmit}) => {
	const classes = useStyles();

	const [state, setState] = useState({
		author: "",
		message: "",
		image: null,
	});

	const submitFormHandler = e => {
		e.preventDefault();
		const formData = new FormData();
		Object.keys(state).forEach(key => {
			formData.append(key, state[key]);
		});

		onSubmit(formData);
	};

	const inputChangeHandler = e => {
		const name = e.target.name;
		const value = e.target.value;
		setState(prevState => {
			return {...prevState, [name]: value};
		});
	};

	const fileChangeHandler = e  => {
		const name = e.target.name;
		const file = e.target.files[0];
		setState(prevState => ({
			...prevState,
			[name]: file
		}));
	};

	return (
		<Grid
			container
			direction="column"
			spacing={2}
			component="form"
			className={classes.root}
			autoComplete="off"
			onSubmit={submitFormHandler}
		>
			<Grid item xs>
				<TextField
					fullWidth
					variant="outlined"
					name="author"
					label="Author"
					value={state.author}
					onChange={inputChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<TextField
					fullWidth
					multiline
					required
					rows={3}
					variant="outlined"
					name="message"
					label="Message"
					value={state.message}
					onChange={inputChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<FileInput
					label="Avatar"
					name="image"
					onChange={fileChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<Button type="submit" color="primary" variant="contained">Create</Button>
			</Grid>
		</Grid>
	);
};

export default MessageForm;